from preprocess_data import clean_df, dataset_maker
from topic_ranking.ranking import token_counter, rank_counts
import collections
import json


# Gets populated by the downloaded artifact in the pipeline
DATASET = 'questions/dataset-questions.csv'

COLORS = ["hsl(0, 80%, 50%)", "hsl(0, 0%, 20%)", "hsl(0, 0%, 40%)",  "hsl(0, 0%, 70%)", "hsl(0, 0%, 90%)"]


def clean_df(dataset):
    df = pd.read_csv(dataset, encoding="ISO-8859-1")
    df_questions = df.copy().drop(['Unnamed: 0'], axis=1)

    return df_questions


def dataset_maker(corpus_dataframe=None):
    corpus = corpus_dataframe.to_numpy()
    corpus = [word for word_list in corpus for word in (word_list)]
    return corpus


def tf_idf(text):
    """ Ranks the words and sentences using TF-IDF
        1. Create Vector representation of the text Object
        2. Pass the text to the object fit and transform it to TF-IDF values
        3. Convert TF-IDF Object to ndarray
        4. Sum up along the whole corpus tf idf values columns it changes the shape to unidimensional tuple
           and gives the total contribution to the corpus (number)
        4. Get top results
    """

    with open(STOP_WORDS_PATH) as csv_stop_words:
        csv_reader = csv.reader(csv_stop_words, delimiter=',')
        stop_words_custom = []

        for words in csv_reader:
            stop_words_custom.extend(words)

    vectorizer = TfidfVectorizer(ngram_range=(1, 2), token_pattern=u'(?ui)\\b\\w*[a-z]+\\w*\\b',
                                 stop_words=stop_words_custom, strip_accents="unicode")
    text_tfidf = vectorizer.fit_transform(text).toarray()
    features = vectorizer.get_feature_names_out()
    sums = text_tfidf.sum(axis=0)
    ranking_data = []
    for column, token in enumerate(features):
        ranking_data.append((token, sums[column]))

    return ranking_data


def sort_list(tf_idf_rank):
    ord_list = []

    for x in tf_idf_rank:
        if (x[1] > 0.6) and (x[1] < 0.7):
            ord_list.append(x)

    sorted_topics = sorted(ord_list, key=lambda x: x[1], reverse=True)
    return sorted_topics


def unique_topics_importance(sorted_topics):
    unique_numbers = set()
    unique_tuples = []

    for t in sorted_topics:
        if t[1] not in unique_numbers:
            unique_numbers.add(t[1])
            unique_tuples.append(t)

    return unique_tuples


def response_maker(unique_tuples):
    data = []
    for i, element in enumerate(unique_tuples):
        if i <= 5:
            data.append({'word': element[0], 'count': element[1], 'Color': COLORS[i]})

    json_string = json.dumps(data, indent=4)
    with open("api_response.json", "w") as outfile:
        outfile.write(json_string)

    return data


def pipeline():
    # 1.
    df = clean_df(DATASET)
    # 2.
    corpus = df['title_prc_bigram'].map(ast.literal_eval)
    # 3.
    corpus = dataset_maker(corpus_dataframe=corpus)
    # 4. TF-IDF
    rank_tfidf = tf_idf(corpus)
    # 5. Sort output
    rank_tfidf = sort_list(rank_tfidf)

    # 6. Unique topics importance
    unique_topics = unique_topics_importance(rank_tfidf)

    # 7
    data = response_maker(unique_topics)

    return data

if __name__ == "__main__":
    data = pipeline()