import json

OUTPUT_JSON = "api_response.json"


def lambda_handler(event, context):
    try:
        with open(OUTPUT_JSON, 'r') as file:
            json_response = json.load(file)
            # json_response = json.dumps(json_response, indent=4)

        return {'statusCode': 200,
                'body': json.dumps(json_response)

                }
    except FileNotFoundError as e:
        print(f"No json file found")

        return "No response available!!"
